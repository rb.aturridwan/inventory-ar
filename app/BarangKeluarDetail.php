<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Jahondust\ModelLog\Traits\ModelLogging;

class BarangKeluarDetail extends Model
{
    //
    // use ModelLogging;
    protected $table = 'barang_keluar_detail';
    protected $fillable = ['no_faktur','id_barang','jumlah_keluar'];
}
