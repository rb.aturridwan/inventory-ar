<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Jahondust\ModelLog\Traits\ModelLogging;

class Barang extends Model
{
    //
    // use ModelLogging;

    protected $fillable = ['id_category','
    id_suplier',
    'id_brand',
    'id_satuan_barang',
    'kode_barang',
    'nama_barang',
    'slug',
    'stok_barang',
    'gambar_barang',
    'deskripsi',
    'harga_barang'];

    protected $table = 'barang';

    public function kategori(){
        return $this->belongsTo('App\Kategori','id_category');
    }

    public function brand(){
        return $this->belongsTo('App\Brand','id_brand');
    }

    public function suplier(){
        return $this->belongsTo('App\Suplier','id_suplier');
    }

    public function satuan(){
        return $this->belongsTo('App\Satuan','id_satuan');
    }
}
