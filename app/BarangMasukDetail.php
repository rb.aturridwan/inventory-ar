<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Jahondust\ModelLog\Traits\ModelLogging;

class BarangMasukDetail extends Model
{
    //
    // use ModelLogging;
    protected $table = 'barang_masuk_detail';
    protected $fillable = ['no_faktur','id_barang','jumlah_masuk'];
}
