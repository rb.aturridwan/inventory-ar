<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Jahondust\ModelLog\Traits\ModelLogging;

class Kategori extends Model
{
    //
    // use ModelLogging;

    protected $table = 'kategori';

    public function barang(){
        return $this->hasOne('App\Barang','id_category');
    }
}
