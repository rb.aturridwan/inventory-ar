<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Jahondust\ModelLog\Traits\ModelLogging;

class BarangKeluar extends Model
{
    //
    // use ModelLogging;
    protected $table = 'barang_keluar';
    protected $fillable = ['no_faktur','created_at','id_user'];
}
