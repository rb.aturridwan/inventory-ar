-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: inventory-db:3306
-- Waktu pembuatan: 09 Jun 2020 pada 01.06
-- Versi server: 5.7.22-log
-- Versi PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_inventory_ar`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_category` bigint(20) UNSIGNED NOT NULL,
  `id_suplier` bigint(20) UNSIGNED NOT NULL,
  `id_brand` bigint(20) UNSIGNED NOT NULL,
  `id_satuan_barang` bigint(20) UNSIGNED NOT NULL,
  `kode_barang` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stok_barang` int(11) NOT NULL DEFAULT '0',
  `gambar_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qr_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci,
  `harga_barang` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_keluar`
--

CREATE TABLE `barang_keluar` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_faktur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_keluar_detail`
--

CREATE TABLE `barang_keluar_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_faktur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_barang` bigint(20) UNSIGNED NOT NULL,
  `jumlah_keluar` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_faktur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_masuk_detail`
--

CREATE TABLE `barang_masuk_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_faktur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_barang` bigint(20) UNSIGNED NOT NULL,
  `jumlah_masuk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `brand`
--

CREATE TABLE `brand` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_brand` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `brand`
--

INSERT INTO `brand` (`id`, `nama_brand`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Gucci', 'gucci', '2020-06-02 00:17:38', '2020-06-02 00:17:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(56, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(57, 7, 'product_name', 'text', 'Product Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(58, 7, 'product_qty', 'number', 'Product Qty', 1, 1, 1, 1, 1, 1, '{}', 3),
(59, 7, 'product_image', 'multiple_images', 'Product Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(60, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(61, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(62, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(63, 8, 'id_category', 'text', 'Id Category', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"Kategori tidak boleh kosong\"}}}', 2),
(64, 8, 'id_suplier', 'text', 'Id Suplier', 1, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"Suplier tidak boleh kosong\"}}}', 3),
(65, 8, 'id_brand', 'text', 'Id Brand', 1, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"Brand tidak boleh kosong\"}}}', 4),
(66, 8, 'id_satuan_barang', 'text', 'Id Satuan Barang', 1, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"Satuan tidak boleh kosong\"}}}', 5),
(67, 8, 'kode_barang', 'text', 'Kode Barang', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 6),
(68, 8, 'nama_barang', 'text', 'Nama Barang', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 7),
(69, 8, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"nama_barang\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:barang,slug\"}}', 8),
(70, 8, 'stok_barang', 'text', 'Stok Barang', 1, 1, 1, 1, 1, 1, '{}', 11),
(71, 8, 'gambar_barang', 'image', 'Gambar Barang', 1, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 13),
(72, 8, 'qr_code', 'text', 'Qr Code', 0, 0, 0, 0, 0, 0, '{}', 14),
(73, 8, 'deskripsi', 'text_area', 'Deskripsi', 0, 1, 1, 1, 1, 1, '{}', 15),
(74, 8, 'harga_barang', 'number', 'Harga Barang', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"},\"message\":{\"required\":\"Tes\"}}', 16),
(75, 8, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 17),
(76, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 18),
(77, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(78, 9, 'nama_brand', 'text', 'Nama Brand', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(79, 9, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"nama_brand\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:brand,slug\"}}', 3),
(80, 9, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 4),
(81, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(82, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(83, 10, 'nama_kategori', 'text', 'Nama Kategori', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(84, 10, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"nama_kategori\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:kategori,slug\"}}', 3),
(85, 10, 'kode_rak', 'text', 'Kode Rak', 1, 1, 1, 1, 1, 1, '{}', 4),
(86, 10, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 5),
(87, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(93, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(94, 13, 'nama_suplier', 'text', 'Nama Suplier', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(95, 13, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"nama_suplier\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:suplier,slug\"}}', 3),
(96, 13, 'alamat_suplier', 'text_area', 'Alamat Suplier', 0, 1, 1, 1, 1, 1, '{}', 4),
(97, 13, 'kontak_suplier', 'text', 'Kontak Suplier', 0, 1, 1, 1, 1, 1, '{}', 5),
(98, 13, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 6),
(99, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(104, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(105, 14, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"nama_satuan\"}}', 2),
(106, 14, 'nama_satuan', 'text', 'Nama Satuan', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(107, 14, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 4),
(108, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(109, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(110, 15, 'id_user', 'text', 'Id User', 0, 1, 1, 0, 0, 0, '{}', 2),
(111, 15, 'no_faktur', 'text', 'No Faktur', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|unique:barang_keluar,no_faktur\"}}', 3),
(112, 15, 'created_at', 'timestamp', 'Tanggal', 0, 1, 1, 1, 1, 0, '{\"validation\":{\"rule\":\"required\"}}', 4),
(113, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(114, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(115, 17, 'id_user', 'text', 'Id User', 0, 1, 1, 0, 0, 0, '{}', 2),
(116, 17, 'no_faktur', 'text', 'No Faktur', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|unique:barang_masuk,no_faktur\"}}', 3),
(117, 17, 'created_at', 'timestamp', 'Tanggal', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 4),
(118, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(119, 8, 'barang_belongsto_kategori_relationship', 'relationship', 'kategori', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Kategori\",\"table\":\"kategori\",\"type\":\"belongsTo\",\"column\":\"id_category\",\"key\":\"id\",\"label\":\"nama_kategori\",\"pivot_table\":\"barang\",\"pivot\":\"0\",\"taggable\":\"0\"}', 9),
(120, 8, 'barang_belongsto_brand_relationship', 'relationship', 'Brand', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Brand\",\"table\":\"brand\",\"type\":\"belongsTo\",\"column\":\"id_brand\",\"key\":\"id\",\"label\":\"nama_brand\",\"pivot_table\":\"barang\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(121, 8, 'barang_belongsto_satuan_barang_relationship', 'relationship', 'Satuan', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Satuan\",\"table\":\"satuan_barang\",\"type\":\"belongsTo\",\"column\":\"id_satuan_barang\",\"key\":\"id\",\"label\":\"nama_satuan\",\"pivot_table\":\"barang\",\"pivot\":\"0\",\"taggable\":\"0\"}', 12),
(122, 8, 'barang_belongsto_suplier_relationship', 'relationship', 'Suplier', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Suplier\",\"table\":\"suplier\",\"type\":\"belongsTo\",\"column\":\"id_suplier\",\"key\":\"id\",\"label\":\"nama_suplier\",\"pivot_table\":\"barang\",\"pivot\":\"0\",\"taggable\":\"0\"}', 19),
(124, 17, 'barang_masuk_belongsto_user_relationship', 'relationship', 'Operator', 0, 1, 1, 0, 0, 0, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"id_user\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"barang\",\"pivot\":\"0\",\"taggable\":\"0\"}', 6),
(125, 15, 'barang_keluar_belongsto_user_relationship', 'relationship', 'Operator', 0, 1, 1, 0, 0, 0, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"id_user\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"barang\",\"pivot\":\"0\",\"taggable\":\"0\"}', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-05-18 12:39:56', '2020-05-18 12:39:56'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-05-18 12:39:56', '2020-05-18 12:39:56'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-05-18 12:39:56', '2020-05-18 12:39:56'),
(7, 'products', 'products', 'Product', 'Products', NULL, 'App\\Product', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":\"created_at\",\"order_direction\":\"desc\",\"default_search_key\":\"id\"}', '2020-05-18 14:18:22', '2020-05-18 14:18:22'),
(8, 'barang', 'barang', 'Barang', 'Barang', 'voyager-bag', 'App\\Barang', NULL, '\\App\\Http\\Controllers\\Voyager\\ProductsController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-06-01 23:42:51', '2020-06-05 00:50:05'),
(9, 'brand', 'brand', 'Brand', 'Brand', 'voyager-treasure', 'App\\Brand', NULL, '\\App\\Http\\Controllers\\Voyager\\BrandsController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-06-01 23:55:06', '2020-06-03 03:30:22'),
(10, 'kategori', 'kategori', 'Kategori', 'Kategori', 'voyager-categories', 'App\\Kategori', NULL, '\\App\\Http\\Controllers\\Voyager\\CategoriesController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-06-01 23:58:20', '2020-06-03 03:30:42'),
(13, 'suplier', 'suplier', 'Suplier', 'Suplier', 'voyager-company', 'App\\Suplier', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-06-02 00:08:20', '2020-06-03 03:31:07'),
(14, 'satuan_barang', 'satuan-barang', 'Satuan Barang', 'Satuan Barang', 'voyager-tag', 'App\\Satuan', NULL, '\\App\\Http\\Controllers\\Voyager\\ProductUnitsController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-06-03 02:58:11', '2020-06-03 03:44:34'),
(15, 'barang_keluar', 'barang-keluar', 'Barang Keluar', 'Barang Keluar', 'voyager-truck', 'App\\BarangKeluar', NULL, '\\App\\Http\\Controllers\\Voyager\\ProductsOutController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-06-03 03:33:02', '2020-06-06 06:00:40'),
(17, 'barang_masuk', 'barang-masuk', 'Barang Masuk', 'Barang Masuk', 'voyager-list-add', 'App\\BarangMasuk', NULL, '\\App\\Http\\Controllers\\Voyager\\ProductsInController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-06-03 03:47:34', '2020-06-06 06:01:04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_rak` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `nama_kategori`, `slug`, `kode_rak`, `created_at`, `updated_at`) VALUES
(1, 'Pintu', 'pintu', '#001', '2020-06-03 02:55:20', '2020-06-03 02:55:20'),
(2, 'Cermin', 'cermin', '#001', '2020-06-04 16:31:45', '2020-06-04 16:31:45');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-05-18 12:39:56', '2020-05-18 12:39:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-05-18 12:39:56', '2020-05-18 12:39:56', 'voyager.dashboard', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 10, '2020-05-18 12:39:56', '2020-06-03 03:48:34', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 9, '2020-05-18 12:39:56', '2020-06-03 03:48:34', 'voyager.roles.index', NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-05-18 12:39:56', '2020-06-01 23:49:05', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2020-05-18 12:39:56', '2020-06-01 23:49:05', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2020-05-18 12:39:56', '2020-06-01 23:49:05', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-05-18 12:39:56', '2020-06-01 23:49:05', 'voyager.bread.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-05-18 12:40:00', '2020-06-01 23:49:05', 'voyager.hooks', NULL),
(17, 1, 'Barang', '', '_self', 'voyager-bag', '#000000', NULL, 2, '2020-06-01 23:42:51', '2020-06-01 23:49:11', 'voyager.barang.index', 'null'),
(18, 1, 'Brand', '', '_self', 'voyager-treasure', NULL, NULL, 5, '2020-06-01 23:55:06', '2020-06-03 03:48:34', 'voyager.brand.index', NULL),
(19, 1, 'Kategori', '', '_self', 'voyager-categories', '#000000', NULL, 6, '2020-06-01 23:58:21', '2020-06-03 03:48:34', 'voyager.kategori.index', 'null'),
(21, 1, 'Suplier', '', '_self', 'voyager-company', '#000000', NULL, 8, '2020-06-02 00:08:20', '2020-06-03 03:48:34', 'voyager.suplier.index', 'null'),
(22, 1, 'Satuan Barang', '', '_self', 'voyager-tag', '#000000', NULL, 7, '2020-06-03 02:58:12', '2020-06-03 03:53:07', 'voyager.satuan-barang.index', 'null'),
(23, 1, 'Barang Keluar', '', '_self', 'voyager-truck', '#000000', NULL, 4, '2020-06-03 03:33:03', '2020-06-03 03:48:34', 'voyager.barang-keluar.index', 'null'),
(24, 1, 'Barang Masuk', '', '_self', 'voyager-list-add', NULL, NULL, 3, '2020-06-03 03:47:36', '2020-06-03 03:48:34', 'voyager.barang-masuk.index', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2016_01_01_000000_add_voyager_user_fields', 1),
(5, '2016_01_01_000000_create_data_types_table', 1),
(6, '2016_05_19_173453_create_menu_table', 1),
(7, '2016_10_21_190000_create_roles_table', 1),
(8, '2016_10_21_190000_create_settings_table', 1),
(9, '2016_11_30_135954_create_permission_table', 1),
(10, '2016_11_30_141208_create_permission_role_table', 1),
(11, '2016_12_26_201236_data_types__add__server_side', 1),
(12, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(13, '2017_01_14_005015_create_translations_table', 1),
(14, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(15, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(16, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(17, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(18, '2017_08_05_000000_add_group_to_settings_table', 1),
(19, '2017_11_26_013050_add_user_role_relationship', 1),
(20, '2017_11_26_015000_create_user_roles_table', 1),
(21, '2018_03_11_000000_add_user_settings', 1),
(22, '2018_03_14_000000_add_details_to_data_types_table', 1),
(23, '2018_03_16_000000_make_settings_value_nullable', 1),
(24, '2019_08_19_000000_create_failed_jobs_table', 1),
(25, '2016_01_01_000000_create_pages_table', 2),
(26, '2016_01_01_000000_create_posts_table', 2),
(27, '2016_02_15_204651_create_categories_table', 2),
(28, '2017_04_11_000000_alter_post_nullable_fields_table', 2),
(29, '2020_05_27_073335_create_product_table', 3),
(30, '2020_05_27_074648_create_kategoris_table', 3),
(31, '2020_05_27_074812_create_brands_table', 3),
(32, '2020_05_27_075030_create_satuans_table', 3),
(33, '2020_05_27_075229_create_supliers_table', 3),
(34, '2020_05_27_081526_create_barang_masuks_table', 3),
(35, '2020_05_27_234318_create_barang_masuk_details_table', 3),
(36, '2020_05_28_000016_create_barang_keluars_table', 3),
(37, '2020_05_28_000059_create_barang_keluar_details_table', 3),
(38, '2020_06_03_031351_change_product_qr_code_to_nullable', 4),
(39, '2020_06_05_010617_change_id_user_barang_masuk_to_nullable', 5),
(41, '2020_06_05_014022_change_id_barang_masuk_to_no_faktur', 6),
(42, '2020_06_06_053416_change_barang_keluar_id_user_to_nullable', 7),
(43, '2020_06_06_053612_change_id_barang_keluar_to_no_faktur', 7);

-- --------------------------------------------------------

--
-- Struktur dari tabel `model_log`
--

CREATE TABLE `model_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `row_id` bigint(20) UNSIGNED NOT NULL,
  `event` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `before` text COLLATE utf8mb4_unicode_ci,
  `after` text COLLATE utf8mb4_unicode_ci,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-05-18 12:39:56', '2020-05-18 12:39:56'),
(2, 'browse_bread', NULL, '2020-05-18 12:39:56', '2020-05-18 12:39:56'),
(3, 'browse_database', NULL, '2020-05-18 12:39:56', '2020-05-18 12:39:56'),
(4, 'browse_media', NULL, '2020-05-18 12:39:56', '2020-05-18 12:39:56'),
(5, 'browse_compass', NULL, '2020-05-18 12:39:56', '2020-05-18 12:39:56'),
(6, 'browse_menus', 'menus', '2020-05-18 12:39:56', '2020-05-18 12:39:56'),
(7, 'read_menus', 'menus', '2020-05-18 12:39:56', '2020-05-18 12:39:56'),
(8, 'edit_menus', 'menus', '2020-05-18 12:39:56', '2020-05-18 12:39:56'),
(9, 'add_menus', 'menus', '2020-05-18 12:39:56', '2020-05-18 12:39:56'),
(10, 'delete_menus', 'menus', '2020-05-18 12:39:56', '2020-05-18 12:39:56'),
(11, 'browse_roles', 'roles', '2020-05-18 12:39:56', '2020-05-18 12:39:56'),
(12, 'read_roles', 'roles', '2020-05-18 12:39:57', '2020-05-18 12:39:57'),
(13, 'edit_roles', 'roles', '2020-05-18 12:39:57', '2020-05-18 12:39:57'),
(14, 'add_roles', 'roles', '2020-05-18 12:39:57', '2020-05-18 12:39:57'),
(15, 'delete_roles', 'roles', '2020-05-18 12:39:57', '2020-05-18 12:39:57'),
(16, 'browse_users', 'users', '2020-05-18 12:39:57', '2020-05-18 12:39:57'),
(17, 'read_users', 'users', '2020-05-18 12:39:57', '2020-05-18 12:39:57'),
(18, 'edit_users', 'users', '2020-05-18 12:39:57', '2020-05-18 12:39:57'),
(19, 'add_users', 'users', '2020-05-18 12:39:57', '2020-05-18 12:39:57'),
(20, 'delete_users', 'users', '2020-05-18 12:39:57', '2020-05-18 12:39:57'),
(21, 'browse_settings', 'settings', '2020-05-18 12:39:57', '2020-05-18 12:39:57'),
(22, 'read_settings', 'settings', '2020-05-18 12:39:57', '2020-05-18 12:39:57'),
(23, 'edit_settings', 'settings', '2020-05-18 12:39:57', '2020-05-18 12:39:57'),
(24, 'add_settings', 'settings', '2020-05-18 12:39:57', '2020-05-18 12:39:57'),
(25, 'delete_settings', 'settings', '2020-05-18 12:39:57', '2020-05-18 12:39:57'),
(41, 'browse_hooks', NULL, '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(42, 'browse_model_log', 'model_log', '2020-05-18 13:21:06', '2020-05-18 13:21:06'),
(43, 'clear_model_log', 'model_log', '2020-05-18 13:21:06', '2020-05-18 13:21:06'),
(44, 'browse_products', 'products', '2020-05-18 14:18:22', '2020-05-18 14:18:22'),
(45, 'read_products', 'products', '2020-05-18 14:18:22', '2020-05-18 14:18:22'),
(46, 'edit_products', 'products', '2020-05-18 14:18:23', '2020-05-18 14:18:23'),
(47, 'add_products', 'products', '2020-05-18 14:18:23', '2020-05-18 14:18:23'),
(48, 'delete_products', 'products', '2020-05-18 14:18:23', '2020-05-18 14:18:23'),
(49, 'browse_barang', 'barang', '2020-06-01 23:42:51', '2020-06-01 23:42:51'),
(50, 'read_barang', 'barang', '2020-06-01 23:42:51', '2020-06-01 23:42:51'),
(51, 'edit_barang', 'barang', '2020-06-01 23:42:51', '2020-06-01 23:42:51'),
(52, 'add_barang', 'barang', '2020-06-01 23:42:51', '2020-06-01 23:42:51'),
(53, 'delete_barang', 'barang', '2020-06-01 23:42:51', '2020-06-01 23:42:51'),
(54, 'browse_brand', 'brand', '2020-06-01 23:55:06', '2020-06-01 23:55:06'),
(55, 'read_brand', 'brand', '2020-06-01 23:55:06', '2020-06-01 23:55:06'),
(56, 'edit_brand', 'brand', '2020-06-01 23:55:06', '2020-06-01 23:55:06'),
(57, 'add_brand', 'brand', '2020-06-01 23:55:06', '2020-06-01 23:55:06'),
(58, 'delete_brand', 'brand', '2020-06-01 23:55:06', '2020-06-01 23:55:06'),
(59, 'browse_kategori', 'kategori', '2020-06-01 23:58:21', '2020-06-01 23:58:21'),
(60, 'read_kategori', 'kategori', '2020-06-01 23:58:21', '2020-06-01 23:58:21'),
(61, 'edit_kategori', 'kategori', '2020-06-01 23:58:21', '2020-06-01 23:58:21'),
(62, 'add_kategori', 'kategori', '2020-06-01 23:58:21', '2020-06-01 23:58:21'),
(63, 'delete_kategori', 'kategori', '2020-06-01 23:58:21', '2020-06-01 23:58:21'),
(69, 'browse_suplier', 'suplier', '2020-06-02 00:08:20', '2020-06-02 00:08:20'),
(70, 'read_suplier', 'suplier', '2020-06-02 00:08:20', '2020-06-02 00:08:20'),
(71, 'edit_suplier', 'suplier', '2020-06-02 00:08:20', '2020-06-02 00:08:20'),
(72, 'add_suplier', 'suplier', '2020-06-02 00:08:20', '2020-06-02 00:08:20'),
(73, 'delete_suplier', 'suplier', '2020-06-02 00:08:20', '2020-06-02 00:08:20'),
(74, 'browse_satuan_barang', 'satuan_barang', '2020-06-03 02:58:12', '2020-06-03 02:58:12'),
(75, 'read_satuan_barang', 'satuan_barang', '2020-06-03 02:58:12', '2020-06-03 02:58:12'),
(76, 'edit_satuan_barang', 'satuan_barang', '2020-06-03 02:58:12', '2020-06-03 02:58:12'),
(77, 'add_satuan_barang', 'satuan_barang', '2020-06-03 02:58:12', '2020-06-03 02:58:12'),
(78, 'delete_satuan_barang', 'satuan_barang', '2020-06-03 02:58:12', '2020-06-03 02:58:12'),
(79, 'browse_barang_keluar', 'barang_keluar', '2020-06-03 03:33:02', '2020-06-03 03:33:02'),
(80, 'read_barang_keluar', 'barang_keluar', '2020-06-03 03:33:02', '2020-06-03 03:33:02'),
(81, 'edit_barang_keluar', 'barang_keluar', '2020-06-03 03:33:02', '2020-06-03 03:33:02'),
(82, 'add_barang_keluar', 'barang_keluar', '2020-06-03 03:33:03', '2020-06-03 03:33:03'),
(83, 'delete_barang_keluar', 'barang_keluar', '2020-06-03 03:33:03', '2020-06-03 03:33:03'),
(84, 'browse_barang_masuk', 'barang_masuk', '2020-06-03 03:47:35', '2020-06-03 03:47:35'),
(85, 'read_barang_masuk', 'barang_masuk', '2020-06-03 03:47:35', '2020-06-03 03:47:35'),
(86, 'edit_barang_masuk', 'barang_masuk', '2020-06-03 03:47:35', '2020-06-03 03:47:35'),
(87, 'add_barang_masuk', 'barang_masuk', '2020-06-03 03:47:35', '2020-06-03 03:47:35'),
(88, 'delete_barang_masuk', 'barang_masuk', '2020-06-03 03:47:35', '2020-06-03 03:47:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(6, 2),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(49, 2),
(50, 1),
(50, 2),
(51, 1),
(51, 2),
(52, 1),
(52, 2),
(53, 1),
(53, 2),
(54, 1),
(54, 2),
(55, 1),
(55, 2),
(56, 1),
(56, 2),
(57, 1),
(57, 2),
(58, 1),
(58, 2),
(59, 1),
(59, 2),
(60, 1),
(60, 2),
(61, 1),
(61, 2),
(62, 1),
(62, 2),
(63, 1),
(63, 2),
(69, 1),
(69, 2),
(70, 1),
(70, 2),
(71, 1),
(71, 2),
(72, 1),
(72, 2),
(73, 1),
(73, 2),
(74, 1),
(74, 2),
(75, 1),
(75, 2),
(76, 1),
(76, 2),
(77, 1),
(77, 2),
(78, 1),
(78, 2),
(79, 1),
(79, 2),
(80, 1),
(80, 2),
(82, 1),
(82, 2),
(83, 1),
(83, 2),
(84, 1),
(84, 2),
(85, 1),
(85, 2),
(87, 1),
(87, 2),
(88, 1),
(88, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_qty` int(11) NOT NULL DEFAULT '0',
  `product_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id`, `product_name`, `product_qty`, `product_image`, `created_at`, `updated_at`) VALUES
(1, 'Gaming Mouse G304', 4, NULL, '2020-05-18 14:19:00', '2020-05-27 07:17:07'),
(2, 'Macbook Pro MD101 Mid 2012', 2, NULL, '2020-05-18 15:43:00', '2020-05-27 07:16:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-05-18 12:39:56', '2020-05-18 12:39:56'),
(2, 'user', 'Normal User', '2020-05-18 12:39:56', '2020-05-18 12:39:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `satuan_barang`
--

CREATE TABLE `satuan_barang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_satuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `satuan_barang`
--

INSERT INTO `satuan_barang` (`id`, `slug`, `nama_satuan`, `created_at`, `updated_at`) VALUES
(1, 'pcs', 'Pcs', '2020-06-03 02:58:45', '2020-06-03 02:58:45'),
(2, 'kg', 'Kg', '2020-06-06 10:05:07', '2020-06-06 10:05:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings/June2020/aGfnHadx7jm3GizHRXTz.jpg', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Inventory AR', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Selamat datang di Inventory AR', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings/June2020/dqCKA4JO0SHnAPnb9X8C.png', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings/June2020/FMN3PCsr9CAk4ttoxPPw.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `suplier`
--

CREATE TABLE `suplier` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_suplier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat_suplier` text COLLATE utf8mb4_unicode_ci,
  `kontak_suplier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `suplier`
--

INSERT INTO `suplier` (`id`, `nama_suplier`, `slug`, `alamat_suplier`, `kontak_suplier`, `created_at`, `updated_at`) VALUES
(1, 'Distro-Gucci', 'distro-gucci', NULL, NULL, '2020-06-03 02:55:54', '2020-06-03 02:55:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2020-05-18 12:39:59', '2020-05-18 12:39:59'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2020-05-18 12:39:59', '2020-05-18 12:39:59'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2020-05-18 12:39:59', '2020-05-18 12:39:59'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2020-05-18 12:39:59', '2020-05-18 12:39:59'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2020-05-18 12:39:59', '2020-05-18 12:39:59'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2020-05-18 12:39:59', '2020-05-18 12:39:59'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2020-05-18 12:39:59', '2020-05-18 12:39:59'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2020-05-18 12:39:59', '2020-05-18 12:39:59'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2020-05-18 12:39:59', '2020-05-18 12:39:59'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2020-05-18 12:40:00', '2020-05-18 12:40:00'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2020-05-18 12:40:00', '2020-05-18 12:40:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@inventory.ar', 'users/default.png', NULL, '$2y$10$EsojJGfuTT4bo5CLJUIJr.4M2XhIbL5bfoVQK6xCaB2pK1DqphSoq', '7HWrRuaGwD2OPujfIL7j9j0l2YUWilJCXqhua9xgRJY5dX7X0wPLsDdhA1TB', '{\"locale\":\"en\"}', '2020-05-18 12:39:59', '2020-05-18 13:29:47'),
(2, 2, 'Operator', 'operator@inventory.ar', 'users/default.png', NULL, '$2y$10$jkZGjyfnsWReF4PZiZz7yeWC/.0pKMfdFpg/W9WsGQlKgLKHlneRW', NULL, '{\"locale\":\"en\"}', '2020-06-06 06:22:53', '2020-06-06 06:22:53');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `barang_kode_barang_unique` (`kode_barang`);

--
-- Indeks untuk tabel `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `barang_keluar_detail`
--
ALTER TABLE `barang_keluar_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `barang_masuk_detail`
--
ALTER TABLE `barang_masuk_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indeks untuk tabel `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indeks untuk tabel `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `model_log`
--
ALTER TABLE `model_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_log_row_id_index` (`row_id`),
  ADD KEY `model_log_event_index` (`event`),
  ADD KEY `model_log_user_id_index` (`user_id`);

--
-- Indeks untuk tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indeks untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indeks untuk tabel `satuan_barang`
--
ALTER TABLE `satuan_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indeks untuk tabel `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indeks untuk tabel `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `barang`
--
ALTER TABLE `barang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `barang_keluar`
--
ALTER TABLE `barang_keluar`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `barang_keluar_detail`
--
ALTER TABLE `barang_keluar_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `barang_masuk_detail`
--
ALTER TABLE `barang_masuk_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `brand`
--
ALTER TABLE `brand`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT untuk tabel `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT untuk tabel `model_log`
--
ALTER TABLE `model_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `satuan_barang`
--
ALTER TABLE `satuan_barang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `suplier`
--
ALTER TABLE `suplier`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ketidakleluasaan untuk tabel `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
