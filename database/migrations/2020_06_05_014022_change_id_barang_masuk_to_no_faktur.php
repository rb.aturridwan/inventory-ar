<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeIdBarangMasukToNoFaktur extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('barang_masuk_detail', function (Blueprint $table) {
            //
            $table->dropColumn('id_barang_masuk');
            $table->string('no_faktur')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_masuk_detail', function (Blueprint $table) {
            //
            $table->dropColumn('no_faktur');
            $table->unsignedBigInteger('id_barang_masuk')->after('id');
        });
    }
}
