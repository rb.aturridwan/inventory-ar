<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_category');
            $table->unsignedBigInteger('id_suplier');
            $table->unsignedBigInteger('id_brand');
            $table->unsignedBigInteger('id_satuan_barang');
            $table->string('kode_barang',10)->unique();
            $table->string('nama_barang');
            $table->string('slug');
            $table->integer('stok_barang')->nullable(false)->default(0);
            $table->string('gambar_barang');
            $table->string('qr_code');
            $table->text('deskripsi')->nullable();
            $table->integer('harga_barang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang');
    }
}
