# INVENTORY

<img src="https://gitlab.com/rb.aturridwan/inventory-ar/-/raw/master/public/storage/settings/June2020/FMN3PCsr9CAk4ttoxPPw.png" height="300" />
<br>

# Setup Project
## Install Docker - <span style='color:red;font-style:italic'>skip this step if already installed</span>

1. Go to  [http://docker.com/get-started](https://www.docker.com/get-started) to download and install docker 
2. open then click next until finish.

## Install GIT Version Control - <span style='color:red;font-style:italic'>skip this step if already installed</span>
1. Download git from http://git-scm.com/download
2. install.

## Clone the project from this page
1. Open terminal (linux, ubuntu, mac) or cmd /command promt (windows), it is optional if you wanna open at specify folder.
2. type command below and then press enter :
    ```
    git clone https://gitlab.com/rb.aturridwan/inventory-ar.git
    ```

## Setup Server
1. type command below to go to project folder and then press enter :
    ```
    cd inventory-ar
    ```
2. type command below to copy laravel environment file from backup and then press enter :
    ```
    cp .local.env .env
    ```
3. type command below to setup our server container (docker) and then press enter :
    ```
    docker-compose up -d
    ```
    <span style="color:red;">run with winpty if you are use windows :</span>
    ```
    winpty docker-compose up -d
    ```
4. Make a coup of coffee, relax, bro - prepare your internet quota. 


## Install Project and Import Database
1. type command below and then press enter to install the project:
    ```
    docker-compose exec app composer install --no-scripts
    ```
    *note : make sure use winpty if using windows.*
     <br>

2. Open browser and go to http://127.0.0.1:3001 or http://localhost:3001

3. Click Import

4. Choose a file from project folder :
    ```
    db_inventory.sql
    ```
5. Click Go.

## Go Take a Walk On Your Project
1. Go to browser
2. Open http://127.0.0.1:8001 or http://localhost:8001
3. Login with an account :
    ```
    Admin :
        email       : admin@inventory.ar
        password    : password

    Operator :
        email       : operator@inventory.ar
        password    : password


## Notes ( optional ) :
### Change port from 8001 to any of it
1. open docker-compose.yml file
2. change port 8001 to anything you want
